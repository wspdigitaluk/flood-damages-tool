﻿# -*- coding: utf-8 -*-

import arcpy, os, datetime, ctypes, uuid
from datetime import date

class Toolbox(object):
	def __init__(self):
		"""Define the toolbox (the name of the toolbox is the name of the .pyt file)."""
		self.label = "Toolbox"
		self.alias = ""

		# List of tool classes associated with this toolbox
		self.tools = [CalculateAndReport]


class Utility(object):
	def countFeatures(self, fc):
		result = arcpy.GetCount_management(fc)
		count = int(result.getOutput(0))
		return count
		
	def findElementInList(self, theList, theElement):
		try:
			index_element = theList.index(theElement)
			return index_element
		except ValueError:
			return None
			
	def folderIsWritable(self, folderpath):
		filepath = folderpath + "\\" + uuid.uuid4().hex + ".tmp"
		try:
			filehandle = open( filepath, 'w' )
			filehandle.close()
		except IOError:
			return False
		
		os.remove(filepath)
		
		return True
		
	def projectIsOpen(self, projectName):
		
		return False
		
		currentFolder = os.path.dirname(os.path.abspath(__file__))
		projectPath = currentFolder + "\\" + projectName

		currentProject = arcpy.mp.ArcGISProject("CURRENT")

		if projectPath == currentProject.filePath:
			return True
		
		return False

			
class FloodEconomicsSettings(object):
	
	def __init__(self):
		self.siteName = ""
		self.filter = ""
		
		self.siteNameField = "Site_Name"

		self.propertyDamagesFC = "DCC.gdb/Property_Damages"
		self.roadDamagesFC = "DCC.gdb/Road_Delay_Damages"
		self.railDamagesFC = "DCC.gdb/Railway_Delay_Damages"
		
		self.mainMapName = "Main Map"
		self.layoutName = "Layout1"
		self.project = "FRISP Workspace.aprx"

		# These are all the fields to sum in the Property Damages points dataset
		
		self.propertyFields = [
		"SW_PVD", "FZ_PVD", 
		"SW_OM2_Mod_0_2", "SW_OM2_Int_0_2", "SW_OM2_Sig_0_2", "SW_OM2_Vsig_0_2",
		"SW_OM2_Mod_0_3", "SW_OM2_Int_0_3", "SW_OM2_Sig_0_3", "SW_OM2_Vsig_0_3",
		"SW_OM2_Mod_0_45", "SW_OM2_Int_0_45", "SW_OM2_Sig_0_45", "SW_OM2_Vsig_0_45",
		"FZ_OM2_Mod_0_2", "FZ_OM2_Int_0_2", "FZ_OM2_Sig_0_2", "FZ_OM2_Vsig_0_2",
		"FZ_OM2_Mod_0_3", "FZ_OM2_Int_0_3", "FZ_OM2_Sig_0_3", "FZ_OM2_Vsig_0_3",
		"FZ_OM2_Mod_0_45", "FZ_OM2_Int_0_45", "FZ_OM2_Sig_0_45", "FZ_OM2_Vsig_0_45",
		"SW_OM1a", "SW_OM1b", "SW_OM2a", "SW_OM2b", "FZ_OM1a", "FZ_OM1b", "FZ_OM2a", "FZ_OM2b",  
		"FZ_FCERM_GiA", "SW_FCERM_GiA", "Max_FCERM_GiA",
		"Max_OM1a", "Max_OM1b", "Max_OM2a", "Max_OM2b",
		"Max_OM2_Mod_0_2", "Max_OM2_Int_0_2", "Max_OM2_Sig_0_2", "Max_OM2_Vsig_0_2",
		"Max_OM2_Mod_0_3", "Max_OM2_Int_0_3", "Max_OM2_Sig_0_3", "Max_OM2_Vsig_0_3",
		"Max_OM2_Mod_0_45", "Max_OM2_Int_0_45", "Max_OM2_Sig_0_45", "Max_OM2_Vsig_0_45",
		"PFR_SW_PVD", "PFR_SW_OM1a", "PFR_SW_OM1b", "PFR_SW_OM2a", "PFR_SW_GiA",
		"PFR_FZ_PVD", "PFR_FZ_OM1a", "PFR_FZ_OM1b", "PFR_FZ_OM2a", "PFR_FZ_GiA",
		"Sum_OM2"
		]

		# These are all the fields to sum in the Road Damages line dataset
		self.roadFields = ["SW_PVD", "SW_GiA", "FZ_PVD", "FZ_GiA", "Max_PVD", "Max_GiA"]

		# These are all the fields to find the max in the Railway Damages line dataset
		self.railwayFields = ["SW_PVD", "SW_GiA", "FZ_PVD", "FZ_GiA", "Max_PVD", "Max_GiA"]
			
		# This is for mapping fields in the Road Damages lines dataset to the Sites polygon dataset fields (where the name is different)
		self.siteRoadFieldMapping = {
			"SW_PVD" : "Rd_SW_PVD",
			"SW_GiA" : "Rd_SW_GiA",
			"FZ_PVD" : "Rd_FZ_PVD",
			"FZ_GiA" : "Rd_FZ_GiA",
			"Max_PVD" : "Rd_Max_PVD",
			"Max_GiA" : "Rd_Max_GiA"
		}
		
		# This is for mapping fields in the Railway Damages lines dataset to the Sites polygon dataset fields (where the name is different)
		self.siteRailwayFieldMapping = {
			"SW_PVD" : "Rw_SW_PVD",
			"SW_GiA" : "Rw_SW_GiA",
			"FZ_PVD" : "Rw_FZ_PVD",
			"FZ_GiA" : "Rw_FZ_GiA",
			"Max_PVD" : "Rw_Max_PVD",
			"Max_GiA" : "Rw_Max_GiA"
		}
		
		self.sitePropertyFields = self.propertyFields
		self.siteRoadFields = self.mapFields(self.roadFields, self.siteRoadFieldMapping)
		self.siteRailwayFields = self.mapFields(self.railwayFields, self.siteRailwayFieldMapping)

	def setSiteName(self, siteName):
		self.siteName = siteName
		self.filter = self.siteNameField + " = '" + self.siteName + "'"
		
	def mapFields(self, originalFields, fieldMapping):
		newList = []
		for field in originalFields:
			if field in fieldMapping:
				newList.append(fieldMapping[field])
			else:
				newList.append(field)
		return newList


class FloodEconomics(object):
	
	def __init__(self):
		self.settings = FloodEconomicsSettings()
		self.util = Utility()
	
	def UpdateSiteCalcs(self, calcFeatures, sourceClipFC):
		if calcFeatures == "ROAD":
			sourceFields = self.settings.roadFields
			calcType = "SUM"
			siteFields = self.settings.siteRoadFields
		elif calcFeatures == "RAIL":
			sourceFields = self.settings.railwayFields
			calcType = "MAX"
			siteFields = self.settings.siteRailwayFields
		else:
			sourceFields = self.settings.propertyFields
			calcType = "SUM"
			siteFields = self.settings.sitePropertyFields
	
		sumFields = []
		
		for field in sourceFields:
			sumFields.append([field, calcType]) 
		
		sumPropertyTable = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)
		arcpy.Statistics_analysis(sourceClipFC, sumPropertyTable, sumFields)

		sumPropertyFields = self.getCalcFieldNames(sourceFields, calcType)

		arcpy.AddMessage("sourceFields: " + str(sourceFields))
		arcpy.AddMessage("calcType: " + str(calcType))
		arcpy.AddMessage("siteFields: " + str(siteFields))

		with arcpy.da.SearchCursor(sumPropertyTable, sumPropertyFields) as sumCursor:
			# Return the first row only
			sumRow = sumCursor.next()

		arcpy.AddMessage("Fields: " + str(siteFields))
		arcpy.AddMessage("Filter: " + str(self.settings.filter))

		with arcpy.da.UpdateCursor(self.settings.siteBoundaryFC, siteFields, self.settings.filter) as upCursor:
			for row in upCursor:
				i = 0
				for fieldValue in sumRow:
					# Add the sums to the site polygon feature
					arcpy.AddMessage("Found value: " + str(fieldValue))

					if fieldValue is None:
						row[i] = 0
					else:
						row[i] = round(fieldValue)
					
					i = i + 1

				# Update the cursor with the updated list
				upCursor.updateRow(row)
			
		return
	
	def CalculateRisk(self, siteBoundaries, siteName):
		
		
		self.settings.setSiteName(siteName)
		self.settings.siteBoundaryFC = siteBoundaries
		
		arcpy.AddMessage("Calculating risk for site: " + str(siteName))
	
		# Select polygon
		selectFC = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)
		arcpy.Select_analysis(siteBoundaries, selectFC, self.settings.filter)
		
		# Perform clip of Property, Road and Rail damages (Feature Classes to be as constants)
		clipPropertyFC = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)
		arcpy.Clip_analysis(self.settings.propertyDamagesFC, selectFC, clipPropertyFC)

		# Get the unique waterbodies that are in the clipped properties data
		waterbodies = ''
		unique_vals = [x[0] for x in arcpy.da.TableToNumPyArray(clipPropertyFC, "wb_name")]
		wb_names = set(unique_vals)
		for wb in wb_names:
			if wb == 'Not part of a river WB catchment':
				wb = 'Coastal catchment'
			elif wb == '':
				pass
			arcpy.AddMessage("Waterbody: " + str(wb))
			waterbodies = waterbodies + str(wb) + ', '
		if waterbodies[:2] == ', ':
			waterbodies = waterbodies[2:]
		waterbodies = waterbodies[:-2]

		
		if 'Not part of a river WB catchment' in wb_names:
			arcpy.AddMessage("Includes 'Not part of a river WB catchment'")
		
		# Add the waterbodies string to the site polygon
		with arcpy.da.UpdateCursor(self.settings.siteBoundaryFC, "wb_name", self.settings.filter) as upCursor:
			for row in upCursor:
				row[0] = waterbodies
				upCursor.updateRow(row)
		
		clipRoadFC = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)
		arcpy.Clip_analysis(self.settings.roadDamagesFC, selectFC, clipRoadFC)
		
		clipRailwayFC = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)
		arcpy.Clip_analysis(self.settings.railDamagesFC, selectFC, clipRailwayFC)

		# Count the number of features in the site area
		util = Utility()
		
		numProperties = util.countFeatures(clipPropertyFC)
		numRoads = util.countFeatures(clipRoadFC)
		numRailways = util.countFeatures(clipRailwayFC)
		

		
		arcpy.AddMessage("numProperties: " + str(numProperties))
		arcpy.AddMessage("numRoads: " + str(numRoads))
		arcpy.AddMessage("numRailways: " + str(numRailways))		
		
		### Property sums
		
		if numProperties > 0:
			self.UpdateSiteCalcs("PROPERTY", clipPropertyFC)
		else:
			self.SetFieldValue(self.settings.siteBoundaryFC, self.settings.filter, self.settings.sitePropertyFields, 0)

		### Road sums
		
		if numRoads > 0:
			self.UpdateSiteCalcs("ROAD", clipRoadFC)
		else:
			self.SetFieldValue(self.settings.siteBoundaryFC, self.settings.filter, self.settings.siteRoadFields, 0)
		
		### Railway sums
		
		if numRailways > 0:
			self.UpdateSiteCalcs("RAIL", clipRailwayFC)
		else:
			self.SetFieldValue(self.settings.siteBoundaryFC, self.settings.filter, self.settings.siteRailwayFields, 0)
		
		return
		
	def SetFieldValue(self, featureClass, filter, fields, value):
	
		arcpy.AddMessage("Updating " + str(featureClass) + " using filter " + str(filter) + " adding " + str(value) + " to " + str(fields))
	
		with arcpy.da.UpdateCursor(featureClass, fields, filter) as upCursor:
			for row in upCursor:
				i = 0
				for field in fields:
					row[i] = 0
					i = i + 1

				# Update the cursor with the updated list
				upCursor.updateRow(row)
		return
	
	def getCalcFieldNames(self, fieldList, calcType):
		newList = []
		for field in fieldList:
			newList.append(calcType + "_" + field)
		return newList

class CalculateAndReport(object):
	def __init__(self):
		"""Define the tool (tool name is the name of the class)."""
		self.label = "Calculate Risk and Create Report (Single Site)"
		self.description = "Generate risk calculations and create report for a polygon area"
		self.canRunInBackground = True
		self.settings = FloodEconomicsSettings()
		self.util = Utility()

	def getParameterInfo(self):
		
		param0 = arcpy.Parameter(
			displayName="Site Boundaries Data",
			name="site_boundaries_data",
			datatype="DEFeatureClass",
			parameterType="Required",
			direction="Input")

		param0.filter.list = ["Polygon"]  # Want only Polygon Feature Classes
		
		param1 = arcpy.Parameter(
			displayName="Site to Print",
			name="site_to_print",
			datatype="GPString",
			parameterType="Required",
			direction="Input")

		param1.filter.type = "ValueList"
		param1.filter.list = [" "]
		
		param2 = arcpy.Parameter(
			displayName="Output Folder",
			name="output_folder",
			datatype="DEFolder",
			parameterType="Required",
			direction="Input")
	
		param3 = arcpy.Parameter(
			displayName="",
			name="",
			datatype="String",
			parameterType="Required",
			direction="Input",
			enabled="False")
		param3.value = "This tool cannot be run if the '" + self.settings.project + "' is open.";
	
		
		params = [param0, param1, param2, param3]
	
		return params

	def isLicensed(self):
		"""Set whether tool is licensed to execute."""
		return True

	def updateParameters(self, parameters):
		"""Modify the values and properties of parameters before internal
		validation is performed.  This method is called whenever a parameter
		has been changed."""
		
		
		parameters[0].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[1].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[2].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[3].enabled = self.util.projectIsOpen(self.settings.project)
		
		if parameters[3].enabled == True:
			return
		
		
		inFC = parameters[0].valueAsText
		
		if inFC != None:
		
			if not arcpy.Exists(inFC):
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			if wkid != 27700 and wkid != 4326:
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			
			if len(fields) == 1:
			
				site_list = []
			
				fc = parameters[0].valueAsText
				
				with arcpy.da.SearchCursor(fc, [self.settings.siteNameField, 'SHAPE@AREA']) as cursor:
					for row in cursor:
						site_name = row[0]
						area = row[1]
						# Add to site list if site name is not null and the site has an area
						if area is not None and site_name is not None and area > 0:
							site_list.append(site_name)
				
				if site_list != None and len(site_list) > 0:
				
					site_list.sort()
					parameters[1].filter.list = site_list
				
					if not parameters[1].value in site_list:
						parameters[1].value = site_list[0]
				else:
						parameters[1].value = ""
		
			else:
				parameters[1].filter.list = []
				parameters[1].value = ""
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				parameters[1].filter.list = []
				parameters[1].value = ""
				
		else:
			parameters[1].filter.list = [" "]
		
		
		
		
		return

	def updateMessages(self, parameters):
		"""Modify the messages created by internal validation for each tool
		parameter.  This method is called after internal validation."""

		if parameters[3].enabled == True:
			parameters[3].setErrorMessage("Please close the project " + self.settings.project + " before running the tool.")
			return
		
		inFC = parameters[0].valueAsText
		outFolder = parameters[2].valueAsText
		
		if inFC != None:
			errs = []
		
			if not arcpy.Exists(inFC):
				parameters[0].setErrorMessage("Feature Class " + inFC + " not found.")
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			fcName = os.path.basename(os.path.normpath(inFC))
			
			if not (wkid == 27700 or wkid == 4326):
				errs.append("'" + fcName + "' does not have a British National Grid or WGS 84 spatial reference. WKID is: " + str(wkid))
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			if len(fields) != 1:
				errs.append("Required field '" + self.settings.siteNameField + "' not found in '" + fcName + "'")
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				errs.append("'" + fcName + "' does not contain any polygons.")
					
			if len(errs) > 0:
				errMsg = "The following errors were encountered: \n\n"
				for err in errs:
					errMsg = errMsg + err + "\n\n" 
					
				parameters[0].setErrorMessage(errMsg)
		
		if outFolder != None:
			if not self.util.folderIsWritable(outFolder):
				parameters[2].setErrorMessage("Cannot output to folder " + outFolder)
		
		return

	def execute(self, parameters, messages):
		
		siteBoundaries = parameters[0].valueAsText
		siteName = parameters[1].valueAsText
		outFolder = parameters[2].valueAsText
	
		fe = FloodEconomics()
		fe.CalculateRisk(siteBoundaries, siteName)

		fer = FloodEconomicsReport()
		fer.createPDF(siteBoundaries, siteName, outFolder)


class CalculateRisk(object):

	def __init__(self):
		"""Define the tool (tool name is the name of the class)."""
		self.label = "Calculate Risk (Single Site)"
		self.description = "Generate risk calculations for a polygon area"
		self.canRunInBackground = True
		self.settings = FloodEconomicsSettings()
		self.util = Utility()
		
	def getParameterInfo(self):
		
		param0 = arcpy.Parameter(
			displayName="Site Boundaries Data",
			name="site_boundaries_data",
			datatype="DEFeatureClass",
			parameterType="Required",
			direction="Input")

		param0.filter.list = ["Polygon"]  # Want only Polygon Feature Classes
		
		param1 = arcpy.Parameter(
			displayName="Site to Calculate",
			name="site_to_calculate",
			datatype="GPString",
			parameterType="Required",
			direction="Input")

		param1.filter.type = "ValueList"
		param1.filter.list = [" "]
		
		params = [param0, param1]
        
		return params

	def isLicensed(self):
		"""Set whether tool is licensed to execute."""
		return True

	def updateParameters(self, parameters):
		"""Modify the values and properties of parameters before internal
		validation is performed.  This method is called whenever a parameter
		has been changed."""
		
		parameters[0].enabled = True
		
		inFC = parameters[0].valueAsText
		
		if inFC != None:
		
			if not arcpy.Exists(inFC):
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			if wkid != 27700 and wkid != 4326:
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			
			if len(fields) == 1:
			
				site_list = []
			
				fc = parameters[0].valueAsText
				
				with arcpy.da.SearchCursor(fc, [self.settings.siteNameField, 'SHAPE@AREA']) as cursor:
					for row in cursor:
						site_name = row[0]
						area = row[1]
						# Add to site list if site name is not null and the site has an area
						if area is not None and site_name is not None and area > 0:
							site_list.append(site_name)
				
				if len(site_list) > 0:
				
					site_list.sort()
					parameters[1].filter.list = site_list
				
					if not parameters[1].value in site_list:
						parameters[1].value = site_list[0]
				else:
						parameters[1].value = ""
		
			else:
				parameters[1].filter.list = []
				parameters[1].value = ""
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				parameters[1].filter.list = []
				parameters[1].value = ""
				
		else:
			parameters[1].filter.list = [" "]
		
		return

	def updateMessages(self, parameters):
		"""Modify the messages created by internal validation for each tool
		parameter.  This method is called after internal validation."""
			
		inFC = parameters[0].valueAsText
	
		if inFC != None:
			errs = []
		
			if not arcpy.Exists(inFC):
				parameters[0].setErrorMessage("Feature Class " + inFC + " not found.")
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			fcName = os.path.basename(os.path.normpath(inFC))
			
			if not (wkid == 27700 or wkid == 4326):
				errs.append("'" + fcName + "' does not have a British National Grid or WGS 84 spatial reference. WKID is: " + str(wkid))
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			if len(fields) != 1:
				errs.append("Required field '" + self.settings.siteNameField + "' not found in '" + fcName + "'")
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				errs.append("'" + fcName + "' does not contain any polygons.")
					
			if len(errs) > 0:
				errMsg = "The following errors were encountered: \n\n"
				for err in errs:
					errMsg = errMsg + err + "\n\n" 
					
				parameters[0].setErrorMessage(errMsg)
	
		return

	def execute(self, parameters, messages):
		
		siteBoundaries = parameters[0].value
		siteName = parameters[1].value
	
		fe = FloodEconomics()
		fe.CalculateRisk(siteBoundaries, siteName)

class CalculateRiskMulti(object):

	def __init__(self):
		"""Define the tool (tool name is the name of the class)."""
		self.label = "Calculate Risk (All Sites)"
		self.description = "Generate risk calculations for all areas"
		self.canRunInBackground = True
		self.settings = FloodEconomicsSettings()
		self.util = Utility()
		
	def getParameterInfo(self):
		
		param0 = arcpy.Parameter(
			displayName="Site Boundaries Data",
			name="site_boundaries_data",
			datatype="DEFeatureClass",
			parameterType="Required",
			direction="Input")

		param0.filter.list = ["Polygon"]  # Want only Polygon Feature Classes
		
		params = [param0]
        
		return params

	def isLicensed(self):
		"""Set whether tool is licensed to execute."""
		return True

	def updateParameters(self, parameters):
		"""Modify the values and properties of parameters before internal
		validation is performed.  This method is called whenever a parameter
		has been changed."""
		
		parameters[0].enabled = True


	def updateMessages(self, parameters):
		"""Modify the messages created by internal validation for each tool
		parameter.  This method is called after internal validation."""
			
		inFC = parameters[0].valueAsText
	
		if inFC != None:
			errs = []
		
			if not arcpy.Exists(inFC):
				parameters[0].setErrorMessage("Feature Class " + inFC + " not found.")
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			fcName = os.path.basename(os.path.normpath(inFC))
			
			if not (wkid == 27700 or wkid == 4326):
				errs.append("'" + fcName + "' does not have a British National Grid or WGS 84 spatial reference. WKID is: " + str(wkid))
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			if len(fields) != 1:
				errs.append("Required field '" + self.settings.siteNameField + "' not found in '" + fcName + "'")
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				errs.append("'" + fcName + "' does not contain any polygons.")
					
			if len(errs) > 0:
				errMsg = "The following errors were encountered: \n\n"
				for err in errs:
					errMsg = errMsg + err + "\n\n" 
					
				parameters[0].setErrorMessage(errMsg)
	
		return

	def execute(self, parameters, messages):
		
		fe = FloodEconomics()
		
		siteBoundaries = parameters[0].value
		#siteName = parameters[1].value
		
		# Calculate risk for every feature in the sites Feature Class
		
		with arcpy.da.SearchCursor(siteBoundaries, self.settings.siteNameField) as cursor:
			for row in cursor:
			
				siteName = row[0]
			
				arcpy.AddMessage("Calculating " + siteName + " ...")
			
				fe.CalculateRisk(siteBoundaries, siteName)

class FloodRiskReport(object):

	def __init__(self):
		"""Define the tool (tool name is the name of the class)."""
		self.label = "Flood Risk Report (Single Site)"
		self.description = "Creates a Flood Risk Report for a polygon area"
		self.canRunInBackground = True
		
		#self.checkCurrentProject()
	
		self.util = Utility()
		self.settings = FloodEconomicsSettings()

	def getParameterInfo(self):
		
		param0 = arcpy.Parameter(
			displayName="Site Boundaries Data",
			name="site_boundaries_data",
			datatype="DEFeatureClass",
			parameterType="Required",
			direction="Input")

		param0.filter.list = ["Polygon"]  # Want only Polygon Feature Classes
		
		param1 = arcpy.Parameter(
			displayName="Site to Print",
			name="site_to_print",
			datatype="GPString",
			parameterType="Required",
			direction="Input")

		param1.filter.type = "ValueList"
		param1.filter.list = [" "]
		
		param2 = arcpy.Parameter(
			displayName="Output Folder",
			name="output_folder",
			datatype="DEFolder",
			parameterType="Required",
			direction="Input")
	
		param3 = arcpy.Parameter(
			displayName="",
			name="",
			datatype="String",
			parameterType="Required",
			direction="Input",
			enabled="False")
		param3.value = "This tool cannot be run if the '" + self.settings.project + "' is open.";
	
		
		params = [param0, param1, param2, param3]
		
		return params

	def isLicensed(self):
		"""Set whether tool is licensed to execute."""
		return True

	def updateParameters(self, parameters):
		"""Modify the values and properties of parameters before internal
		validation is performed.  This method is called whenever a parameter
		has been changed."""
		
		
		parameters[0].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[1].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[2].enabled = not self.util.projectIsOpen(self.settings.project)
		parameters[3].enabled = self.util.projectIsOpen(self.settings.project)
		
		if parameters[3].enabled == True:
			return
		
		
		inFC = parameters[0].valueAsText
		
		if inFC != None:
		
			if not arcpy.Exists(inFC):
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			if wkid != 27700 and wkid != 4326:
				parameters[1].filter.list = []
				parameters[1].value = ""
				return
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			
			if len(fields) == 1:
			
				site_list = []
			
				fc = parameters[0].valueAsText
				
				with arcpy.da.SearchCursor(fc, [self.settings.siteNameField, 'SHAPE@AREA']) as cursor:
					for row in cursor:
						site_name = row[0]
						area = row[1]
						# Add to site list if site name is not null and the site has an area
						if area is not None and site_name is not None and area > 0:
							site_list.append(site_name)
				
				if site_list != None and len(site_list) > 0:
				
					site_list.sort()
					parameters[1].filter.list = site_list
				
					if not parameters[1].value in site_list:
						parameters[1].value = site_list[0]
				else:
						parameters[1].value = ""
		
			else:
				parameters[1].filter.list = []
				parameters[1].value = ""
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				parameters[1].filter.list = []
				parameters[1].value = ""
				
		else:
			parameters[1].filter.list = [" "]
		
		
		
		
		return

	def updateMessages(self, parameters):
		"""Modify the messages created by internal validation for each tool
		parameter.  This method is called after internal validation."""
		
		if parameters[3].enabled == True:
			parameters[3].setErrorMessage("Please close the project " + self.settings.project + " before running the tool.")
			return
		
		inFC = parameters[0].valueAsText
		outFolder = parameters[2].valueAsText
		
		if inFC != None:
			errs = []
		
			if not arcpy.Exists(inFC):
				parameters[0].setErrorMessage("Feature Class " + inFC + " not found.")
				return
		
			wkid = arcpy.Describe(inFC).spatialReference.factoryCode
			
			fcName = os.path.basename(os.path.normpath(inFC))
			
			if not (wkid == 27700 or wkid == 4326):
				errs.append("'" + fcName + "' does not have a British National Grid or WGS 84 spatial reference. WKID is: " + str(wkid))
		
			fields = arcpy.ListFields(inFC, self.settings.siteNameField)
			if len(fields) != 1:
				errs.append("Required field '" + self.settings.siteNameField + "' not found in '" + fcName + "'")
		
			numFeatures = self.util.countFeatures(parameters[0].valueAsText)
		
			if numFeatures < 1:
				errs.append("'" + fcName + "' does not contain any polygons.")
					
			if len(errs) > 0:
				errMsg = "The following errors were encountered: \n\n"
				for err in errs:
					errMsg = errMsg + err + "\n\n" 
					
				parameters[0].setErrorMessage(errMsg)
		
		if outFolder != None:
			if not self.util.folderIsWritable(outFolder):
				parameters[2].setErrorMessage("Cannot output to folder " + outFolder)
		
		return

	def execute(self, parameters, messages):
		siteBoundaries = parameters[0].valueAsText
		siteName = parameters[1].valueAsText
		outFolder = parameters[2].valueAsText

		fer = FloodEconomicsReport()
		fer.createPDF(siteBoundaries, siteName, outFolder)
	

class FloodEconomicsReport(object):

	def __init__(self):
		self.util = Utility()
		self.settings = FloodEconomicsSettings()

	def createPDF(self, siteBoundaries, siteName, outFolder):
	
		"""The source code of the tool."""
		
		arcpy.env.addOutputsToMap = False
		
		
		currentFolder = os.path.dirname(os.path.abspath(__file__))
		projectPath = currentFolder + "\\" + self.settings.project
		
		if not (os.path.exists(projectPath)):
			arcpy.AddError("Cannot find required project file '" + projectPath + "'")
			return
		
		aprx = arcpy.mp.ArcGISProject(projectPath) #"CURRENT"
		#currentProject = arcpy.mp.ArcGISProject("CURRENT")

		#if aprx.filePath == currentProject.filePath:
		#	arcpy.AddError("Please close the project " + self.settings.project + " before running the tool.")
		#	return
		
		# Get the main map, layout and title page
		m = aprx.listMaps(self.settings.mainMapName)[0]
		lyt = aprx.listLayouts(self.settings.layoutName)[0]
		titleLyt = aprx.listLayouts("Title Page")[0]
		
		layer = m.listLayers("Site Boundary")[0]
		layer.definitionQuery = self.settings.siteNameField + " = '" + siteName + "'"
		layer.visible = True
		
		## ** Print layout series
		pages = []
		pages.append("FCERM Funding Potential - Grant in Aid Summary")
		pages.append("FCERM Funding Potential - Fluvial/Coastal Summary")
		pages.append("FCERM Funding Potential - Surface Water Summary")
		pages.append("PFR Funding Potential - Property Flood Resilience Summary")
		pages.append("Historic Flooding")
		pages.append("Topography")
		pages.append("Local Plan Allocations")
		pages.append("National Designations and Ecology")
		pages.append("Working With Natural Processes (WWNP) Layers")
		
		
		## ** Get filename to save as
		today = date.today()
		filename = siteName + " FRISP " + today.strftime("%Y-%m-%d") + ".pdf"
		arcpy.AddMessage("Creating output " + filename + " ....")
		savedFile = outFolder + "\\" + filename
		
		if not self.removeIfExists(savedFile):
			arcpy.AddError("Cannot continue.")
			return
		
		pdfDoc = arcpy.mp.PDFDocumentCreate(savedFile)

		# Get the Site Name polygon details
		site_poly = self.getFeature(siteBoundaries, siteName)
		centroid = site_poly[0]
		extent = site_poly[1].extent
		
		# Values for Economic Summary dictionary....
		valueDict = { }
		valueDict["FZ_PVD"] = int(site_poly[5]/1000)
		valueDict["Rd_FZ_PVD"] = int(site_poly[6]/1000)
		valueDict["Rw_FZ_PVD"] = int(site_poly[7]/1000)
		valueDict["FZ_OM1a"] = int(site_poly[8]/1000)
		valueDict["FZ_OM1b"] = int(site_poly[9]/1000)
		valueDict["FZ_OM2a"] = int(site_poly[10]/1000)
		valueDict["FZ_OM2b"] = int(site_poly[11]/1000)
		valueDict["SW_PVD"] = int(site_poly[12]/1000)
		valueDict["Rd_SW_PVD"] = int(site_poly[13]/1000)
		valueDict["Rw_SW_PVD"] = int(site_poly[14]/1000)
		valueDict["SW_OM1a"] = int(site_poly[15]/1000)
		valueDict["SW_OM1b"] = int(site_poly[16]/1000)
		valueDict["SW_OM2a"] = int(site_poly[17]/1000)
		valueDict["SW_OM2b"] = int(site_poly[18]/1000)
		valueDict["Rd_SW_GiA"] = int(site_poly[19]/1000)
		valueDict["Rw_SW_GiA"] = int(site_poly[20]/1000)
		valueDict["Rd_FZ_GiA"] = int(site_poly[21]/1000)
		valueDict["Rw_FZ_GiA"] = int(site_poly[22]/1000)
		valueDict["FZ_OM2_Mod_0_2"] = site_poly[23]
		valueDict["FZ_OM2_Int_0_2"] = site_poly[24]
		valueDict["FZ_OM2_Sig_0_2"] = site_poly[25]
		valueDict["FZ_OM2_Vsig_0_2"] = site_poly[26]
		valueDict["FZ_OM2_Mod_0_3"] = site_poly[27]
		valueDict["FZ_OM2_Int_0_3"] = site_poly[28]
		valueDict["FZ_OM2_Sig_0_3"] = site_poly[29]
		valueDict["FZ_OM2_Vsig_0_3"] = site_poly[30]
		valueDict["FZ_OM2_Mod_0_45"] = site_poly[31]
		valueDict["FZ_OM2_Int_0_45"] = site_poly[32]
		valueDict["FZ_OM2_Sig_0_45"] = site_poly[33]
		valueDict["FZ_OM2_Vsig_0_45"] = site_poly[34]
		valueDict["SW_OM2_Mod_0_2"] = site_poly[35]
		valueDict["SW_OM2_Int_0_2"] = site_poly[36]
		valueDict["SW_OM2_Sig_0_2"] = site_poly[37]
		valueDict["SW_OM2_Vsig_0_2"] = site_poly[38]
		valueDict["SW_OM2_Mod_0_3"] = site_poly[39]
		valueDict["SW_OM2_Int_0_3"] = site_poly[40]
		valueDict["SW_OM2_Sig_0_3"] = site_poly[41]
		valueDict["SW_OM2_Vsig_0_3"] = site_poly[42]
		valueDict["SW_OM2_Mod_0_45"] = site_poly[43]
		valueDict["SW_OM2_Int_0_45"] = site_poly[44]
		valueDict["SW_OM2_Sig_0_45"] = site_poly[45]
		valueDict["SW_OM2_Vsig_0_45"] = site_poly[46]
		valueDict["PFR_FZ_PVD"] = int(site_poly[47]/1000)
		valueDict["PFR_FZ_OM1a"] = int(site_poly[48]/1000)
		valueDict["PFR_FZ_OM1b"] = int(site_poly[49]/1000)
		valueDict["PFR_FZ_OM2a"] = int(site_poly[50]/1000)
		valueDict["PFR_SW_PVD"] = int(site_poly[51]/1000)
		valueDict["PFR_SW_OM1a"] = int(site_poly[52]/1000)
		valueDict["PFR_SW_OM1b"] = int(site_poly[53]/1000)
		valueDict["PFR_SW_OM2a"] = int(site_poly[54]/1000)
		valueDict["Max_OM2_Mod_0_2"] = site_poly[55]
		valueDict["Max_OM2_Int_0_2"] = site_poly[56]
		valueDict["Max_OM2_Sig_0_2"] = site_poly[57]
		valueDict["Max_OM2_Vsig_0_2"] = site_poly[58]
		valueDict["Max_OM2_Mod_0_3"] = site_poly[59]
		valueDict["Max_OM2_Int_0_3"] = site_poly[60]
		valueDict["Max_OM2_Sig_0_3"] = site_poly[61]
		valueDict["Max_OM2_Vsig_0_3"] = site_poly[62]
		valueDict["Max_OM2_Mod_0_45"] = site_poly[63]
		valueDict["Max_OM2_Int_0_45"] = site_poly[64]
		valueDict["Max_OM2_Sig_0_45"] = site_poly[65]
		valueDict["Max_OM2_Vsig_0_45"] = site_poly[66]
		valueDict["Sum_OM2"] = site_poly[67]
		
		###################
		##### Calcs!! #####
		###################
		
		#### Page 1 Calculations ####
		
		# Potential OM1a - Surface Water
		valueDict["SW_OM1_Calc"] = valueDict["SW_OM1a"] + valueDict["SW_OM1b"] + valueDict["Rd_SW_GiA"] + valueDict["Rw_SW_GiA"]
		
		# Potential GiA - Fluvial
		valueDict["Potential_FZ_GiA"] = valueDict["FZ_OM1a"] + valueDict["FZ_OM1b"] + valueDict["FZ_OM2a"] + valueDict["FZ_OM2b"]

		# Potential GiA - Surface Water
		valueDict["Potential_SW_GiA"] = valueDict["SW_OM1_Calc"] + valueDict["SW_OM2a"] + valueDict["SW_OM2b"]

		#### Page 2 Calculations ####

		# Total potential OM1
		valueDict["FZ_Total_Potential_OM1"] = valueDict["FZ_OM1a"] + valueDict["FZ_OM1b"] + valueDict["Rd_FZ_GiA"] + valueDict["Rw_FZ_GiA"]
		
		# Potential GiA
		valueDict["FZ_Potential_GiA"] = valueDict["FZ_OM1a"] + valueDict["FZ_OM1b"] + valueDict["Rd_FZ_GiA"] + valueDict["Rw_FZ_GiA"] + valueDict["FZ_OM2a"] + valueDict["FZ_OM2b"]

		#### Page 3 Calculations ####

		# Total potential OM1
		valueDict["SW_Total_Potential_OM1"] = valueDict["SW_OM1a"] + valueDict["SW_OM1b"] + valueDict["Rd_SW_GiA"] + valueDict["Rw_SW_GiA"]

		# Potential GiA
		valueDict["SW_Potential_GiA"] = valueDict["SW_OM1a"] + valueDict["SW_OM1b"] + valueDict["Rd_SW_GiA"] + valueDict["Rw_SW_GiA"] + valueDict["SW_OM2a"] + valueDict["SW_OM2b"]

		#### Page 4 Calculations

		# Potential GiA - Fluvial
		valueDict["PFR_FZ_Potential_GiA"] = valueDict["PFR_FZ_OM1a"] + valueDict["PFR_FZ_OM1b"] + valueDict["PFR_FZ_OM2a"]

		# Potential GiA - Surface Water
		valueDict["PFR_SW_Potential_GiA"] = valueDict["PFR_SW_OM1a"] + valueDict["PFR_SW_OM1b"] + valueDict["PFR_SW_OM2a"]
	
		
		########### Update all the layouts with values

		for i in range (1, 5):  # 1, 5
			pageSearch = "Page " + str(i) + " - *"
			pageTextElements = lyt.listElements('TEXT_ELEMENT', pageSearch)
			for txtElement in pageTextElements:
				txtElemName = txtElement.name
				dictKey = txtElemName[9:]
				dictValue = round(valueDict[dictKey])
				if dictKey[:7] == "FZ_OM2_" or dictKey[:7] == "SW_OM2_" or dictKey[:8] == "Max_OM2_":
					if dictValue > 0:
						txtElement.text = dictValue
					else:
						txtElement.text = "0" # Workaround because 0 will not be output in the layout!
				else:
					txtElement.text = '£' + f'{dictValue:,}'+'k'

		mf = lyt.listElements('MAPFRAME_ELEMENT', 'Main Map Frame')[0]

		# Give the extent some breathing room
		extentWidth = extent.XMax - extent.XMin
		extentXCentre = extent.XMin + (extentWidth/2)
		newExtentWidth = extentWidth * 1.3
		xMin = extentXCentre - (newExtentWidth/2)
		xMax = extentXCentre + (newExtentWidth/2)
		
		spatRef = arcpy.SpatialReference(27700)
		newExtent = arcpy.Extent(xMin, extent.YMin, xMax, extent.YMax, extent.ZMin, extent.ZMax, extent.MMin, extent.MMax, spatRef)
		mf.camera.setExtent(newExtent)

		catchment_text = ''
		wb_list = site_poly[68]
		if wb_list == 'Coastal catchment':
			catchment_text = 'Coastal'
		elif 'Coastal catchment' not in wb_list:
			catchment_text = 'Fluvial'
		elif 'Coastal catchment' in wb_list and len(wb_list) > 18:
			catchment_text = 'Site covers coastal and fluvial catchments'
		else:
			catchment_text = 'no properties under polygon'

		# Set the catchment text
		txtCatchments = lyt.listElements('TEXT_ELEMENT', 'Catchment text')
		for txtCatchment in txtCatchments:
			txtCatchment.text = catchment_text  # text from the wb_name field
			txtCatchment.visible = True  # Always? Might want to set False for some maps in which case the logic goes further down in the pages loop
		
		# Set the waterbodies text
		txtWaterbodies = lyt.listElements('TEXT_ELEMENT', 'Waterbodies text')
		for txtWaterbody in txtWaterbodies:
			txtWaterbody.text = site_poly[68]  # text from the wb_name field
			txtWaterbody.visible = True  # Always? Might want to set False for some maps in which case the logic goes further down in the pages loop
				
		# Set the date
		txtDate = lyt.listElements('TEXT_ELEMENT', 'Date drawn text')[0]
		txtDate.text = site_poly[4] #formattedDate
		
		# Set the user
		txtDrawnBy = lyt.listElements('TEXT_ELEMENT', 'Drawn by text')[0]
		txtDrawnBy.text = site_poly[3] #user
		
		# Set the Site Reference
		txtSiteRef = lyt.listElements('TEXT_ELEMENT', 'Site Reference text')[0]
		txtSiteRef.text = siteName
		txtSiteRef = titleLyt.listElements('TEXT_ELEMENT', 'Site Reference Value')[0]
		txtSiteRef.text = siteName
		
		# Set the Status
		txtStatus = lyt.listElements('TEXT_ELEMENT', 'Status text')[0]
		txtStatus.text = site_poly[2]
		txtStatus = titleLyt.listElements('TEXT_ELEMENT', 'Status Value')[0]
		txtStatus.text = site_poly[2]
		
		# Set the Site Coordinates
		arcpy.AddMessage("Polygon Centroid - X: " + str(round(centroid[0])) + ", Y: " + str(round(centroid[1])))
		txtSiteCoords = lyt.listElements('TEXT_ELEMENT', 'Site coordinates text')[0]
		txtSiteCoords.text = str(round(centroid[0])) + ", " + str(round(centroid[1]))
		txtSiteCoords = titleLyt.listElements('TEXT_ELEMENT', 'Site Coordinates Value')[0]
		txtSiteCoords.text = str(round(centroid[0])) + ", " + str(round(centroid[1]))
		
		# Set the Date Printed in the title page
		now = datetime.datetime.now()
		formattedDate = now.strftime("%d/%m/%Y")
		txtDatePrinted = titleLyt.listElements('TEXT_ELEMENT', 'Date Printed Value')[0]
		txtDatePrinted.text = formattedDate
		
		# Set the Properties At Risk in the title page
		txtPropertiesAtRisk = titleLyt.listElements('TEXT_ELEMENT', 'Properties At Risk Value')[0]
		txtPropertiesAtRisk.text = str(round(valueDict["Sum_OM2"]))
				
		# Set the Catchment Type in the title page
		txtCatchmentType = titleLyt.listElements('TEXT_ELEMENT', 'Catchment Type Value')[0]
		txtCatchmentType.text = catchment_text

		# Set the Catchment List in the title page
		txtCatchmentList = titleLyt.listElements('TEXT_ELEMENT', 'Catchment List Value')[0]
		txtCatchmentList.text = wb_list
		
		# Set the overview map cross location

		fcPath = arcpy.CreateScratchName(workspace=arcpy.env.scratchGDB)		
		fcName = os.path.basename(fcPath)
		
		sr = arcpy.SpatialReference(27700)
		arcpy.CreateFeatureclass_management(arcpy.env.scratchGDB, fcName, "POINT", spatial_reference=sr)
		point = arcpy.Point(centroid[0], centroid[1])

		om = aprx.listMaps("Overview Map")[0]
		om.addDataFromPath(fcPath)

		lyr = om.listLayers(fcName)[0]
		sym = lyr.symbology
		sym.renderer.symbol.applySymbolFromGallery("Cross 4", 2)
		sym.renderer.symbol.color = {'RGB' : [255, 0, 0, 100]}
		sym.renderer.symbol.size = 48
		lyr.symbology = sym

		cursor = arcpy.da.InsertCursor(fcPath, ["SHAPE@"])
		cursor.insertRow([point])
		del cursor # required to save insert

		# Export Title Page
		exportPDF = outFolder + "\\titlepage.pdf"
		titleLyt.exportToPDF(exportPDF, resolution=400)
		pdfDoc.appendPages(exportPDF)
		self.removeIfExists(exportPDF) # cleanup

		# Create each map in the series
		
		leg = lyt.listElements("LEGEND_ELEMENT", "*")[0]
		
		i = 0
		
		for page in pages:
			i = i + 1

			layerToFind = "Map " + str(i) + " Layers"
			lyr = m.listLayers(layerToFind)[0]
			lyr.visible = True

			leg.syncLayerVisibility = True # Required to refresh the legend correctly

			arcpy.AddMessage("Creating layout from " + layerToFind + " ....")
		
			# Update the Title
			txtTitle = lyt.listElements('TEXT_ELEMENT', 'Deliverable text')[0]
			txtTitle.text = page
			
			# Update the left page elements
			self.updateLeftPageElements(lyt, i)
			
			# Update the copyright
			copyrightTitle = "Copyright Text - Page " + str(i)
			copyrightElem = lyt.listElements('TEXT_ELEMENT', copyrightTitle)[0]
			copyrightText = copyrightElem.text + "Positions shown relative to EPSG:27700 || Printed by: " + self.get_display_name() + " - " + self.current_date_time() + "\n"
			copyrightText = copyrightText + "Project path: " + projectPath
			copyrightElem.text = copyrightText
			copyrightElem.visible = True
			
			# Update the Page Number
			pageNoElem = lyt.listElements('TEXT_ELEMENT', "PageNo text")[0]
			pageNoElem.text = "Page " + str(i) + " of " + str(len(pages))
						
			arcpy.AddMessage("Exporting layout '" + str(i) + "' ....")
			
			# Export layout
			exportPDF = outFolder + "\\page" + str(i) + ".pdf"
		
			lyt.exportToPDF(exportPDF, resolution=400)

			pdfDoc.appendPages(exportPDF)
			
			self.removeIfExists(exportPDF) # cleanup
			
			lyr.visible = False
			copyrightElem.visible = False

		# Commit changes and delete variable reference
		pdfDoc.saveAndClose()
		del pdfDoc

		return
		
	def countFeatures(self, fc):
		result = arcpy.GetCount_management(fc)
		count = int(result.getOutput(0))
		return count
		
	def getFeature(self, fc, siteName):
		arcpy.AddMessage("Obtaining details for site '" + siteName + "' ....")
		whereClause = self.settings.siteNameField + " = '" + siteName + "'"
		
		with arcpy.da.SearchCursor(fc, ['SHAPE@XY', 'SHAPE@', 
		'Status', 'Drawn_by', 'Date',
		'FZ_PVD', 'Rd_FZ_PVD', 'Rw_FZ_PVD',
		'FZ_OM1a', 'FZ_OM1b', 'FZ_OM2a', 'FZ_OM2b',
		'SW_PVD', 'Rd_SW_PVD', 'Rw_SW_PVD',
		'SW_OM1a', 'SW_OM1b', 'SW_OM2a', 'SW_OM2b',
		'Rd_SW_GiA', 'Rw_SW_GiA', 'Rd_FZ_GiA', 'Rw_FZ_GiA',
		'FZ_OM2_Mod_0_2', 'FZ_OM2_Int_0_2', 'FZ_OM2_Sig_0_2', 'FZ_OM2_Vsig_0_2',
		'FZ_OM2_Mod_0_3', 'FZ_OM2_Int_0_3', 'FZ_OM2_Sig_0_3', 'FZ_OM2_Vsig_0_3',
		'FZ_OM2_Mod_0_45', 'FZ_OM2_Int_0_45', 'FZ_OM2_Sig_0_45', 'FZ_OM2_Vsig_0_45',
		'SW_OM2_Mod_0_2', 'SW_OM2_Int_0_2', 'SW_OM2_Sig_0_2', 'SW_OM2_Vsig_0_2',
		'SW_OM2_Mod_0_3', 'SW_OM2_Int_0_3', 'SW_OM2_Sig_0_3', 'SW_OM2_Vsig_0_3',
		'SW_OM2_Mod_0_45', 'SW_OM2_Int_0_45', 'SW_OM2_Sig_0_45', 'SW_OM2_Vsig_0_45',
		'PFR_FZ_PVD', 'PFR_FZ_OM1a', 'PFR_FZ_OM1b', 'PFR_FZ_OM2a',
		'PFR_SW_PVD', 'PFR_SW_OM1a', 'PFR_SW_OM1b', 'PFR_SW_OM2a',
		'Max_OM2_Mod_0_2', 'Max_OM2_Int_0_2', 'Max_OM2_Sig_0_2', 'Max_OM2_Vsig_0_2',
		'Max_OM2_Mod_0_3', 'Max_OM2_Int_0_3', 'Max_OM2_Sig_0_3', 'Max_OM2_Vsig_0_3',
		'Max_OM2_Mod_0_45', 'Max_OM2_Int_0_45', 'Max_OM2_Sig_0_45', 'Max_OM2_Vsig_0_45',
		'Sum_OM2', 'wb_name'
		], whereClause ) as cursor:
			# Return the first polygon only
			row = cursor.next()
			return row

	def get_display_name(self):
		GetUserNameEx = ctypes.windll.secur32.GetUserNameExW
		NameDisplay = 3

		size = ctypes.pointer(ctypes.c_ulong(0))
		GetUserNameEx(NameDisplay, None, size)

		nameBuffer = ctypes.create_unicode_buffer(size.contents.value)
		GetUserNameEx(NameDisplay, nameBuffer, size)
		name = nameBuffer.value
		
		x = name.split(",")
		name = x[1].strip() + " " + x[0].strip()
		return name

	def current_date_time(self):
		now = datetime.datetime.now()
		current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")
		return current_datetime
		
	def removeIfExists(self, filepath):
		if os.path.exists(filepath):
			try:
				os.remove(filepath)
				return True
			except:
				arcpy.AddError("Cannot delete existing file " + filepath)
				return False
	
		return True
		
	def updateLeftPageElements(self, layout, pageNo):
		pageGroups = layout.listElements('GRAPHIC_ELEMENT', 'Left Page Elements - Page*')
		
		for page in pageGroups:
			if page.name == "Left Page Elements - Page " + str(pageNo):
				page.visible = True
			else:
				page.visible = False
	