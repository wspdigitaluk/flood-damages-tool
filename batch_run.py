import arcpy

# Set the workspace
arcpy.env.workspace = "C:/FRISP/DCC.gdb"
arcpy.ImportToolbox("C:/FRISP/DCC Flood Risk v2 - no open project check, display name update.pyt", "frisp")


# Create the search cursor
cursor = arcpy.da.SearchCursor("Site_Boundary", "Site_Name")

# Iterate through the rows in the cursor
for row in cursor:
    print("Running FRISP for: {0}".format(row[0]))
    arcpy.CalculateAndReport_frisp("C:/FRISP/DCC.gdb/Site_Boundary", str(row[0]), "C:/FRISP output")

del cursor, row